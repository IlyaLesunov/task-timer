import React from 'react';

// Импорт компонентов
import SectionTask from './src/sections/tasks/index'

import {
    SafeAreaView, ScrollView,
} from 'react-native';


function App(): JSX.Element {
  return (
    <SafeAreaView style={{ backgroundColor: 'whitesmoke',  flex: 1, paddingTop: 10, marginLeft: 10, marginRight: 10 }}>
        <ScrollView>
            <SectionTask />
        </ScrollView>
    </SafeAreaView>
  );
}

export default App;
