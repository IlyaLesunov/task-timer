import React, { useState } from 'react';
import { View, Text, Modal, StyleSheet, TouchableOpacity } from 'react-native';

const ModalTimer = ({ status }) => {
    const [modalVisible, setModalVisible] = useState(false);
    console.log('status', status)
    return (
        <View style={styles.container}>
            <Modal
                visible={status}
                animationType="slide"
                transparent={ true }
                onRequestClose={() => setModalVisible(false)}
            >
                <View style={styles.modalContainer}>
                    <Text style={styles.textModal}>Модальное окно на весь экран</Text>
                    <TouchableOpacity onPress={() => setModalVisible(false)}>
                        <Text>Закрыть</Text>
                    </TouchableOpacity>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black'
    },
    textModal: {
        color: "white"
    }
});

export default ModalTimer;
