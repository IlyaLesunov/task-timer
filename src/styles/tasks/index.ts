import { StyleSheet } from "react-native";

const styleTaskWrapper = StyleSheet.create({
    container: {
        flex: 1,
        display: 'flex',
        gap: 10
    }
})

const styleTaskItem = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 20,
        paddingBottom: 25,
        display: "flex",
        justifyContent: "center",
        borderRadius: 10,
        boxShadow: '-3px 5px 38px 10px whitesmoke'
    },
    title: {
        fontSize: 18,
        color: 'black',
        fontWeight: '400',
        marginBottom: 7
    },
    description: {
        fontSize: 16,
        color: 'black',
        opacity: 0.5
    },
    status: {
        display: "flex",
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 20,
    },
    statusText: {
        fontSize: 14,
        color: 'red',
        fontWeight: '500'
    },
    statusSpent: {
        fontSize: 15,
        color: 'green',
        fontWeight: '500'
}
})

export {
    styleTaskWrapper,
    styleTaskItem
}
