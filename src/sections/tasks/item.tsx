import React, {useState} from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { styleTaskItem } from '../../styles/tasks'
import ModalTimer from "../../components/Modals/Timer";

const ItemTask = ({ item }:any) => {

    const [modalVisible, setModalVisible] = useState(false);

    const changeModal = () => {
        console.log('changeModal')
        setModalVisible(true)
    }

    return (
        <TouchableOpacity onPress={changeModal} style={{ backgroundColor: 'red', zIndex: 99 }}>
            <View style={ styleTaskItem.container }>
                    <Text  style={ styleTaskItem.title }>{ item.name }</Text>
                    <Text style={ styleTaskItem.description }>
                        { item.description ? item.description : 'Описания нет' }
                    </Text>
                    <View style={ styleTaskItem.status }>
                        <Text style={ styleTaskItem.statusText }> { item.status ? 'Готово' : 'В разработке' } </Text>
                        <Text style={ styleTaskItem.statusSpent }> { item.time_spent } </Text>
                    </View>
                <ModalTimer status={ modalVisible }/>
            </View>
        </TouchableOpacity>
    );
};

export default ItemTask;
