import React, { useState } from 'react';
import { Text }  from 'react-native';
import ItemTask from './item'
import { styleTaskWrapper } from '../../styles/tasks'

import {
    View,
} from 'react-native';


function SectionTask(): JSX.Element {

    const [list, setList] = useState([
        {
            id: 0,
            name: 'Убрать с сайта инфо о ставке с ФЛЭТ',
            description: 'Сейчас пока нет скидок от банков для наших клиентов на процентную ставку . ' +
                'Нужно убрать ставку с нами и платеж с нами',
            status: false,
            time_spent: '7ч'
        },
        {
            id: 1,
            name: 'При падении CRM не отвечал сайт',
            description: '',
            status: false,
            time_spent: '7ч'
        },
    ]);


    return (
        <View style={ styleTaskWrapper.container }>
            { list.map((item) => (
                <ItemTask key={item.id} item={item} />
            ))}
        </View>
    );
}

export default SectionTask;
